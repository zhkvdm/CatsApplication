package com.zhkvdm.catsapplication.feature.cats.domain

import com.zhkvdm.catsapplication.feature.cats.domain.model.CatEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable

interface CatsRepository {
    fun getCats(): Observable<List<CatEntity>>

    fun getFavCats(): Flowable<List<CatEntity>>

    fun setFavCat(cat: CatEntity): Completable

    fun removeFavCat(cat: CatEntity): Completable
}
