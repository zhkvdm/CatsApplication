package com.zhkvdm.catsapplication.feature.cats.domain

import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable

interface CatsInteractor {
    fun getCats(): Observable<List<CatModel>>

    fun getFavCats(): Flowable<List<CatModel>>

    fun setFavCats(cat: CatModel): Completable

    fun removeFavCats(cat: CatModel): Completable
}
