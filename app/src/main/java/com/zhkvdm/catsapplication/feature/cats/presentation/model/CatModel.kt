package com.zhkvdm.catsapplication.feature.cats.presentation.model

data class CatModel(
    val id: String,
    val url: String,
    val isFavourite: Boolean = false,
)
