package com.zhkvdm.catsapplication.feature.cats.presentation.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.AsyncDifferConfig
import com.zhkvdm.catsapplication.R
import com.zhkvdm.catsapplication.base.BaseApplication
import com.zhkvdm.catsapplication.base.utils.ImageSaveUtil
import com.zhkvdm.catsapplication.databinding.FragmentCatsBinding
import com.zhkvdm.catsapplication.feature.cats.di.CatsComponent
import com.zhkvdm.catsapplication.feature.cats.di.DaggerCatsComponent
import com.zhkvdm.catsapplication.feature.cats.presentation.Favourite
import com.zhkvdm.catsapplication.feature.cats.presentation.adapter.CatsAdapter
import com.zhkvdm.catsapplication.feature.cats.presentation.adapter.decorator.VerticalSpaceItemDecoration
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel
import javax.inject.Inject


class FavouriteFragment : Fragment(R.layout.fragment_cats), Favourite.View {
    private var _binding: FragmentCatsBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var presenter: Favourite.Presenter

    @Inject
    lateinit var asyncDifferConfig: AsyncDifferConfig<CatModel>

    private lateinit var catsAdapter: CatsAdapter

    private val component: CatsComponent by lazy(LazyThreadSafetyMode.NONE) {
        DaggerCatsComponent.builder()
            .catsComponentDependencies(
                (requireActivity().application as BaseApplication).appComponent
            )
            .build()
    }

    override fun onAttach(context: Context) {
        component.injects(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentCatsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        setView()
        presenter.onCreated()
    }

    private fun setView() {
        catsAdapter = CatsAdapter(asyncDifferConfig)
        catsAdapter.apply {
            onFavClickCallback = ::onFavClick
            onDownloadClickCallback = ::onDownloadClick
        }
        binding.rvCats.apply {
            adapter = catsAdapter
            addItemDecoration(
                VerticalSpaceItemDecoration(resources.getDimension(R.dimen.size_m).toInt())
            )
        }
    }

    private fun onFavClick(catModel: CatModel) {
        presenter.onFavClick(catModel)
    }

    private fun onDownloadClick(catModel: CatModel) {
        ImageSaveUtil(requireContext()).download(catModel.url)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
        _binding = null
    }

    override fun setCats(catsList: List<CatModel>) {
        catsAdapter.submitList(catsList)
    }

    override fun showError(errResId: Int) {
        Toast.makeText(requireContext(), requireContext().getText(errResId), Toast.LENGTH_SHORT).show()
    }
}