package com.zhkvdm.catsapplication.feature.cats.data.network

import com.zhkvdm.catsapplication.feature.cats.data.network.model.CatDataModel
import com.zhkvdm.catsapplication.base.utils.Constants.API_LIMIT
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface CatsApi {
    @GET("v1/images/search")
    fun getCats(
        @Query("limit") limit: Int = API_LIMIT,
        @Query("api_key") apiKey: String
    ): Observable<List<CatDataModel>>
}
