package com.zhkvdm.catsapplication.feature.cats.data

import com.zhkvdm.catsapplication.feature.cats.data.database.data.CatDao
import com.zhkvdm.catsapplication.feature.cats.data.network.CatsApi
import com.zhkvdm.catsapplication.feature.cats.data.network.mapper.toDbEntity
import com.zhkvdm.catsapplication.feature.cats.data.network.mapper.toEntity
import com.zhkvdm.catsapplication.feature.cats.domain.CatsRepository
import com.zhkvdm.catsapplication.feature.cats.domain.model.CatEntity
import com.zhkvdm.catsapplication.base.utils.Constants.API_KEY
import com.zhkvdm.catsapplication.base.utils.Constants.API_LIMIT
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class CatsRepositoryImpl @Inject constructor(
    private val api: CatsApi,
    private val dao: CatDao,
) : CatsRepository {

    override fun getCats(): Observable<List<CatEntity>> = api.getCats(apiKey = API_KEY).map { cats ->
        cats.map { it.toEntity() }
    }

    override fun getFavCats(): Flowable<List<CatEntity>> {
        return dao.getCats().map { cats ->
            cats.map { it.toEntity() }
        }
    }

    override fun setFavCat(cat: CatEntity): Completable {
        return dao.insertCat(cat.toDbEntity()).subscribeOn(Schedulers.io())
    }

    override fun removeFavCat(cat: CatEntity): Completable {
        return dao.deleteCat(cat.toDbEntity().id).subscribeOn(Schedulers.io())
    }
}
