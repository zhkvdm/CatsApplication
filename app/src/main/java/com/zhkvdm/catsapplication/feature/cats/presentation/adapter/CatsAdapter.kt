package com.zhkvdm.catsapplication.feature.cats.presentation.adapter

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.zhkvdm.catsapplication.R
import com.zhkvdm.catsapplication.databinding.ItemCatBinding
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel

class CatsAdapter(
    asyncDifferConfig: AsyncDifferConfig<CatModel>
) : ListAdapter<CatModel, CatsAdapter.CatViewHolder>(asyncDifferConfig) {

    var loadCallback: (() -> Unit) = { }
    var onFavClickCallback: ((CatModel) -> Unit) = { }
    var onDownloadClickCallback: ((CatModel) -> Unit) = { }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatViewHolder {
        return CatViewHolder(
            ItemCatBinding.bind(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_cat,
                    parent,
                    false
                )
            )
        )
    }

    override fun onBindViewHolder(holder: CatViewHolder, position: Int) {
        holder.onBind(getItem(position))
        if ((position >= itemCount - 1)) {
            loadCallback()
        }
    }

    inner class CatViewHolder(private val binding: ItemCatBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind(item: CatModel) {
            binding.pbLoading.isVisible = true
            binding.ivFavourite.setImageResource(
                if (item.isFavourite) {
                    R.drawable.ic_star_fill
                } else {
                    R.drawable.ic_star
                }
            )
            binding.ivFavourite.setOnClickListener {
                onFavClickCallback(item)
            }
            binding.ivDownload.setOnClickListener {
                onDownloadClickCallback(item)
            }
            Glide.with(binding.root)
                .load(item.url)
                .centerCrop()
                .listener(
                    object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            p0: GlideException?,
                            p1: Any?,
                            p2: Target<Drawable>?,
                            p3: Boolean
                        ): Boolean {
                            binding.pbLoading.isVisible = false
                            binding.ivCat.setImageResource(android.R.drawable.ic_delete)
                            return false
                        }

                        override fun onResourceReady(
                            p0: Drawable?,
                            p1: Any?,
                            p2: Target<Drawable>?,
                            p3: DataSource?,
                            p4: Boolean
                        ): Boolean {
                            binding.pbLoading.isVisible = false
                            return false
                        }
                    }
                )
                .into(binding.ivCat)
        }
    }
}
