package com.zhkvdm.catsapplication.feature.cats.domain.model

data class CatEntity(
    val id: String,
    val url: String,
)
