package com.zhkvdm.catsapplication.feature.cats.domain

import com.zhkvdm.catsapplication.feature.cats.domain.mapper.toEntity
import com.zhkvdm.catsapplication.feature.cats.domain.mapper.toModel
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import javax.inject.Inject

class CatsInteractorImpl @Inject constructor(
    private val repo: CatsRepository,
) : CatsInteractor {
    override fun getCats(): Observable<List<CatModel>> = repo.getCats().map { cats ->
        cats.map { it.toModel() }
    }

    override fun getFavCats(): Flowable<List<CatModel>> =
        repo.getFavCats().map { cats ->
            cats.map { it.toModel(true) }
        }

    override fun setFavCats(cat: CatModel): Completable {
        return repo.setFavCat(cat.toEntity())
    }

    override fun removeFavCats(cat: CatModel): Completable {
        return repo.removeFavCat(cat.toEntity())
    }
}
