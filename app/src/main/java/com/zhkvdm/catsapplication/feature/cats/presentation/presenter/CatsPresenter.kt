package com.zhkvdm.catsapplication.feature.cats.presentation.presenter

import com.zhkvdm.catsapplication.R
import com.zhkvdm.catsapplication.feature.cats.domain.CatsInteractor
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel
import com.zhkvdm.catsapplication.feature.cats.presentation.Cats
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.Observables
import io.reactivex.rxjava3.kotlin.addTo
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class CatsPresenter @Inject constructor(
    private val interactor: CatsInteractor,
) : Cats.Presenter() {

    private var isLoading = false

    private val cats: MutableList<CatModel> = mutableListOf()

    override fun onCreated() {
        if (cats.isEmpty()) {
            initialLoad()
        }
        getFavCats()
    }

    override fun loadMore() {
        if (!isLoading) {
            isLoading = true
            interactor.getCats()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate { isLoading = false }
                .subscribe(this::handleResponse, this::handleError)
                .addTo(viewDisposables)
        }
    }

    private fun initialLoad() {
        Observables.zip(
            interactor.getCats(),
            interactor.getFavCats().toObservable()
        ).subscribe(
            { (cats, favCats) ->
                updateCats(cats, favCats)
            },
            {
                handleError(it)
            }
        ).addTo(viewDisposables)
    }

    private fun getFavCats() {
        interactor.getFavCats()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleFavCatsResponse, this::handleError)
            .addTo(viewDisposables)
    }

    override fun onFavClick(catModel: CatModel) {
        if (catModel.isFavourite) {
            removeFavCats(catModel)
        } else {
            setFavCats(catModel)
        }
    }

    private fun setFavCats(catModel: CatModel) {
        interactor.setFavCats(catModel)
            .subscribe()
            .addTo(viewDisposables)
    }

    private fun removeFavCats(catModel: CatModel) {
        interactor.removeFavCats(catModel)
            .subscribe()
            .addTo(viewDisposables)
    }

    private fun handleResponse(cats: List<CatModel>) {
        this.cats.addAll(cats)
        view?.setCats(this.cats)
    }

    private fun handleFavCatsResponse(favCats: List<CatModel>) {
        updateCats(this.cats, favCats)
    }

    private fun updateCats(cats: List<CatModel>, favCats: List<CatModel>) {
        val list = cats.merge(favCats)
        this.cats.clear()
        this.cats.addAll(list)
        view?.setCats(this.cats)
    }

    private fun handleError(error: Throwable) {
        error.printStackTrace()
        view?.showError(R.string.error)
    }

    private fun List<CatModel>.merge(cats: List<CatModel>) = this.map { cat ->
        cat.copy(isFavourite = cats.map { it.id }.contains(cat.id))
    }
}
