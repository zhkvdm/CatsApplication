package com.zhkvdm.catsapplication.feature.cats.presentation

import androidx.annotation.StringRes
import com.zhkvdm.catsapplication.base.mvp.MvpPresenter
import com.zhkvdm.catsapplication.base.mvp.MvpView
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel

interface Favourite {

    interface View : MvpView {
        fun setCats(catsList: List<CatModel>)
        fun showError(@StringRes errResId: Int)
    }

    abstract class Presenter : MvpPresenter<View>() {
        abstract fun onCreated()
        abstract fun onFavClick(catModel: CatModel)
    }
}
