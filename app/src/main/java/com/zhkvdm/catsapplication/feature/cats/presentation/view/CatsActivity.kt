package com.zhkvdm.catsapplication.feature.cats.presentation.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import com.zhkvdm.catsapplication.R
import com.zhkvdm.catsapplication.databinding.ActivityCatsBinding

class CatsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCatsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCatsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setNavigation()
    }

    private fun setNavigation() {
        val navHostFragment = supportFragmentManager
            .findFragmentById(R.id.navigationHost) as NavHostFragment
        val navController = navHostFragment.navController
        navController.apply {
            addOnDestinationChangedListener { _, destination, _ ->
                supportActionBar?.title = destination.label
            }
        }
    }
}
