package com.zhkvdm.catsapplication.feature.cats.domain.mapper

import com.zhkvdm.catsapplication.feature.cats.domain.model.CatEntity
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel

fun CatEntity.toModel(isFavourite: Boolean = false) = CatModel(
    id = id,
    url = url,
    isFavourite = isFavourite,
)

fun CatModel.toEntity() = CatEntity(
    id = id,
    url = url,
)
