package com.zhkvdm.catsapplication.feature.cats.di

import com.zhkvdm.catsapplication.feature.cats.data.database.data.CatDao
import com.zhkvdm.catsapplication.feature.cats.data.network.CatsApi
import com.zhkvdm.catsapplication.feature.cats.presentation.view.CatsFragment
import com.zhkvdm.catsapplication.feature.cats.presentation.view.FavouriteFragment
import dagger.Component

@Component(
    dependencies = [CatsComponentDependencies::class],
    modules = [CatsModule::class],
)
interface CatsComponent {
    fun injects(target: CatsFragment)
    fun injects(target: FavouriteFragment)
}

interface CatsComponentDependencies {
    fun provideCatsApi(): CatsApi
    fun provideCatsDao(): CatDao
}
