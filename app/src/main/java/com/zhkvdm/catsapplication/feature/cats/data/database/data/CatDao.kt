package com.zhkvdm.catsapplication.feature.cats.data.database.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.zhkvdm.catsapplication.feature.cats.data.database.model.CatEntity
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable

@Dao
interface CatDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCat(cat: CatEntity): Completable

    @Query("SELECT * FROM cats")
    fun getCats(): Flowable<List<CatEntity>>

    @Query("DELETE FROM cats WHERE id = :id")
    fun deleteCat(id: String): Completable
}
