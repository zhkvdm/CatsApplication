package com.zhkvdm.catsapplication.feature.cats.data.network.model

data class CatDataModel(
    val id: String,
    val url: String,
)
