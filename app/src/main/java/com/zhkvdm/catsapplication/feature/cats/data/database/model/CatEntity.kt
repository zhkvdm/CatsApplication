package com.zhkvdm.catsapplication.feature.cats.data.database.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val TABLE_NAME = "cats"

const val COLUMN_ID = "id"
const val COLUMN_URL = "url"

@Entity(tableName = TABLE_NAME)
data class CatEntity(
    @PrimaryKey
    @ColumnInfo(name = COLUMN_ID)
    var id: String,

    @ColumnInfo(name = COLUMN_URL)
    var url: String? = null,
)
