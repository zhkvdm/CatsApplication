package com.zhkvdm.catsapplication.feature.cats.di

import androidx.recyclerview.widget.AsyncDifferConfig
import com.zhkvdm.catsapplication.feature.cats.data.CatsRepositoryImpl
import com.zhkvdm.catsapplication.feature.cats.domain.CatsInteractor
import com.zhkvdm.catsapplication.feature.cats.domain.CatsInteractorImpl
import com.zhkvdm.catsapplication.feature.cats.domain.CatsRepository
import com.zhkvdm.catsapplication.feature.cats.presentation.adapter.CatsDiffUtil
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel
import com.zhkvdm.catsapplication.feature.cats.presentation.presenter.CatsPresenter
import com.zhkvdm.catsapplication.feature.cats.presentation.presenter.FavouritePresenter
import com.zhkvdm.catsapplication.feature.cats.presentation.Cats
import com.zhkvdm.catsapplication.feature.cats.presentation.Favourite
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface CatsModule {

    @Binds
    fun bindCatsPresenter(presenter: CatsPresenter): Cats.Presenter

    @Binds
    fun bindFavouritePresenter(presenter: FavouritePresenter): Favourite.Presenter

    @Binds
    fun bindCatsRepository(repository: CatsRepositoryImpl): CatsRepository

    @Binds
    fun bindCatsInteractor(repository: CatsInteractorImpl): CatsInteractor

    companion object {
        @Provides
        fun provideAsyncDifferConfig(): AsyncDifferConfig<CatModel> =
            AsyncDifferConfig.Builder(CatsDiffUtil())
                .build()
    }
}
