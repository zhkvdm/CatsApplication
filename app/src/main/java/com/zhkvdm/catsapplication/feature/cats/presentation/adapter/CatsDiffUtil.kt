package com.zhkvdm.catsapplication.feature.cats.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel

class CatsDiffUtil : DiffUtil.ItemCallback<CatModel>() {

    override fun areItemsTheSame(oldItem: CatModel, newItem: CatModel) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: CatModel, newItem: CatModel) =
        oldItem.url == newItem.url && oldItem.isFavourite == newItem.isFavourite
}
