package com.zhkvdm.catsapplication.feature.cats.data.network.mapper

import com.zhkvdm.catsapplication.feature.cats.data.network.model.CatDataModel
import com.zhkvdm.catsapplication.feature.cats.domain.model.CatEntity
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel

fun CatDataModel.toEntity() = CatEntity(
    id = id,
    url = url,
)

fun com.zhkvdm.catsapplication.feature.cats.data.database.model.CatEntity.toEntity() = CatEntity(
    id = id,
    url = url.orEmpty(),
)

fun CatEntity.toDbEntity() = com.zhkvdm.catsapplication.feature.cats.data.database.model.CatEntity(
    id = id,
    url = url,
)
