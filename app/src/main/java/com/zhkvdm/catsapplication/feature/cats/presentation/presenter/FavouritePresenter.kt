package com.zhkvdm.catsapplication.feature.cats.presentation.presenter

import com.zhkvdm.catsapplication.R
import com.zhkvdm.catsapplication.feature.cats.domain.CatsInteractor
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel
import com.zhkvdm.catsapplication.feature.cats.presentation.Favourite
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.kotlin.addTo
import javax.inject.Inject

class FavouritePresenter @Inject constructor(
    private val interactor: CatsInteractor,
) : Favourite.Presenter() {

    override fun onCreated() {
        interactor.getFavCats()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::handleResponse, this::handleError)
            .addTo(viewDisposables)
    }

    override fun onFavClick(catModel: CatModel) {
        interactor.removeFavCats(catModel)
            .doOnError(this::handleError)
            .subscribe()
    }

    private fun handleResponse(cats: List<CatModel>) {
        view?.setCats(cats.reversed())
    }

    private fun handleError(error: Throwable) {
        error.printStackTrace()
        view?.showError(R.string.error)
    }
}
