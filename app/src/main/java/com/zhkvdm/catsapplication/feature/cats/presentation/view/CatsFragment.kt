package com.zhkvdm.catsapplication.feature.cats.presentation.view

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.AsyncDifferConfig
import com.zhkvdm.catsapplication.R
import com.zhkvdm.catsapplication.base.BaseApplication
import com.zhkvdm.catsapplication.base.utils.ImageSaveUtil
import com.zhkvdm.catsapplication.databinding.FragmentCatsBinding
import com.zhkvdm.catsapplication.feature.cats.di.CatsComponent
import com.zhkvdm.catsapplication.feature.cats.di.DaggerCatsComponent
import com.zhkvdm.catsapplication.feature.cats.presentation.Cats
import com.zhkvdm.catsapplication.feature.cats.presentation.adapter.CatsAdapter
import com.zhkvdm.catsapplication.feature.cats.presentation.adapter.decorator.VerticalSpaceItemDecoration
import com.zhkvdm.catsapplication.feature.cats.presentation.model.CatModel
import javax.inject.Inject

class CatsFragment : Fragment(R.layout.fragment_cats), Cats.View, MenuProvider {

    private var _binding: FragmentCatsBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var presenter: Cats.Presenter

    @Inject
    lateinit var asyncDifferConfig: AsyncDifferConfig<CatModel>

    private lateinit var catsAdapter: CatsAdapter

    private val component: CatsComponent by lazy(LazyThreadSafetyMode.NONE) {
        DaggerCatsComponent.builder()
            .catsComponentDependencies(
                (requireActivity().application as BaseApplication).appComponent
            )
            .build()
    }

    override fun onAttach(context: Context) {
        component.injects(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentCatsBinding.inflate(inflater, container, false)
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.attachView(this)
        setView()
        presenter.onCreated()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkPermission()
    }

    private fun setView() {
        catsAdapter = CatsAdapter(asyncDifferConfig)
        catsAdapter.apply {
            loadCallback = { presenter.loadMore() }
            onFavClickCallback = ::onFavClick
            onDownloadClickCallback = ::onDownloadClick
        }
        binding.rvCats.apply {
            adapter = catsAdapter
            addItemDecoration(
                VerticalSpaceItemDecoration(resources.getDimension(R.dimen.size_m).toInt())
            )
        }
    }

    private fun onFavClick(catModel: CatModel) {
        presenter.onFavClick(catModel)
    }

    private fun onDownloadClick(catModel: CatModel) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            if (checkPermission()) {
                ImageSaveUtil(requireContext()).download(catModel.url)
            }
        } else {
            ImageSaveUtil(requireContext()).download(catModel.url)
        }
    }

    private fun checkPermission(): Boolean {
        val permission =
            ContextCompat.checkSelfPermission(
                requireContext().applicationContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
        if (permission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )
            return false
        }
        return true
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.detachView()
        _binding = null
    }

    override fun setCats(catsList: List<CatModel>) {
        val c: MutableList<CatModel> = mutableListOf()
        c.addAll(catsList)
        catsAdapter.submitList(c)
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.cats_menu, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            R.id.item_favourite -> {
                val action = CatsFragmentDirections.actionCatsFragmentToFavouriteFragment()
                findNavController().navigate(action)
                false
            }
            else -> false
        }
    }

    override fun showError(errResId: Int) {
        Toast.makeText(requireContext(), requireContext().getText(errResId), Toast.LENGTH_SHORT).show()
    }

    companion object {
        private val PERMISSIONS_STORAGE = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        private const val REQUEST_EXTERNAL_STORAGE = 1
    }
}
