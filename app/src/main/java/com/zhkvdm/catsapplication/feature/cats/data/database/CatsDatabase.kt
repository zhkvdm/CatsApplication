package com.zhkvdm.catsapplication.feature.cats.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.zhkvdm.catsapplication.feature.cats.data.database.data.CatDao
import com.zhkvdm.catsapplication.feature.cats.data.database.model.CatEntity
import com.zhkvdm.catsapplication.base.utils.Constants.DATABASE_VERSION

@Database(version = DATABASE_VERSION, entities = [CatEntity::class])
abstract class CatsDatabase : RoomDatabase() {
    abstract fun catDao(): CatDao
}
