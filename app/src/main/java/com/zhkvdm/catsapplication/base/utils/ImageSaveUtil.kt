package com.zhkvdm.catsapplication.base.utils

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.media.MediaScannerConnection
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.zhkvdm.catsapplication.R
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class ImageSaveUtil(private val context: Context) {

    fun download(url: String) {
        Glide.with(context)
            .load(url)
            .into(object : CustomTarget<Drawable?>() {
                override fun onResourceReady(
                    resource: Drawable,
                    transition: Transition<in Drawable?>?
                ) {
                    val bitmap = (resource as BitmapDrawable).bitmap
                    Toast.makeText(context, context.getText(R.string.saving), Toast.LENGTH_SHORT).show()
                    save(bitmap)
                }

                override fun onLoadCleared(placeholder: Drawable?) = Unit
                override fun onLoadFailed(errorDrawable: Drawable?) {
                    super.onLoadFailed(errorDrawable)
                    Toast.makeText(
                        context,
                        context.getText(R.string.save_error),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
    }

    private fun save(image: Bitmap): Boolean {
        if (isExternalStorageWritable()) {
            if (saveImage(image)) {
                Toast.makeText(context, context.getText(R.string.save_complete), Toast.LENGTH_SHORT).show()
                return true
            }
        }
        Toast.makeText(context, context.getText(R.string.save_error), Toast.LENGTH_SHORT).show()
        return false
    }

    private fun saveImage(finalBitmap: Bitmap): Boolean {
        val timeStamp = SimpleDateFormat(DATE_PATTERN, Locale.getDefault()).format(Date())
        val fileName = "$IMAGE_NAME$timeStamp$IMAGE_TYPE"

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            saveImageNewApi(finalBitmap, fileName)
        } else {
            saveImageDeprecatedApi(finalBitmap, fileName)
        }
    }

    private fun saveImageDeprecatedApi(finalBitmap: Bitmap, fileName: String): Boolean {
        val filePath = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
            IMAGE_DIRECTORY_NAME
        )
        val file = File(filePath, fileName)
        filePath.mkdirs()
        try {
            val out = FileOutputStream(file)
            writeFile(finalBitmap, out)

            MediaScannerConnection.scanFile(
                context, arrayOf(file.toString()),
                arrayOf(file.name), null
            )
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    private fun saveImageNewApi(finalBitmap: Bitmap, fileName: String): Boolean {
        val filePath = Environment.DIRECTORY_PICTURES + File.separator + IMAGE_DIRECTORY_NAME
        val resolver = context.contentResolver
        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
            put(MediaStore.MediaColumns.MIME_TYPE, IMAGE_MIME_TYPE)
            put(MediaStore.MediaColumns.RELATIVE_PATH, filePath)
        }
        var result = false

        try {
            val uri = resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

            uri?.let {
                resolver.openOutputStream(it)?.let { out ->
                    result = writeFile(finalBitmap, out)
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        return result
    }

    private fun isExternalStorageWritable(): Boolean {
        val state: String = Environment.getExternalStorageState()
        return Environment.MEDIA_MOUNTED == state
    }

    @Throws(Exception::class)
    private fun writeFile(finalBitmap: Bitmap, outputStream: OutputStream): Boolean {
        return try {
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
            outputStream.flush()
            outputStream.close()
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    companion object {
        private const val IMAGE_NAME = "Cat_"
        private const val IMAGE_DIRECTORY_NAME = "Cats"
        private const val IMAGE_MIME_TYPE = "image/jpeg"
        private const val IMAGE_TYPE = ".jpg"
        private const val DATE_PATTERN = "yyyyMMdd_HHmmss"
    }
}