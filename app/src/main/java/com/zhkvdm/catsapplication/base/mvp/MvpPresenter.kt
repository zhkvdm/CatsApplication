package com.zhkvdm.catsapplication.base.mvp

import androidx.annotation.UiThread
import io.reactivex.rxjava3.disposables.CompositeDisposable

open class MvpPresenter<V : MvpView> {

    var viewDisposables: CompositeDisposable = CompositeDisposable()

    var view: V? = null

    @UiThread
    fun attachView(view: V) {
        this.view = view
    }

    @UiThread
    fun detachView() {
        view = null
        viewDisposables.clear()
    }
}
