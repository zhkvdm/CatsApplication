package com.zhkvdm.catsapplication.base.di

import android.app.Application
import android.content.Context
import androidx.room.Room
import com.zhkvdm.catsapplication.feature.cats.data.database.CatsDatabase
import com.zhkvdm.catsapplication.feature.cats.data.database.data.CatDao
import com.zhkvdm.catsapplication.base.utils.Constants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object DatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(application: Application): CatsDatabase {
        return Room.databaseBuilder(application, CatsDatabase::class.java, Constants.DATABASE_NAME)
            .build()
    }

    @Provides
    fun provideDao(catsDatabase: CatsDatabase): CatDao {
        return catsDatabase.catDao()
    }
}
