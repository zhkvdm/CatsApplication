package com.zhkvdm.catsapplication.base

import android.app.Application
import com.zhkvdm.catsapplication.base.di.AppComponent
import com.zhkvdm.catsapplication.base.di.AppModule
import com.zhkvdm.catsapplication.base.di.DaggerAppComponent

class BaseApplication : Application() {

    val appComponent: AppComponent = DaggerAppComponent
        .builder()
        .appModule(AppModule(this))
        .build()
}
