package com.zhkvdm.catsapplication.base.di

import com.zhkvdm.catsapplication.base.BaseApplication
import com.zhkvdm.catsapplication.feature.cats.di.CatsComponentDependencies
import dagger.Binds
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        DatabaseModule::class,
        ComponentDependenciesModule::class,
    ]
)
interface AppComponent : CatsComponentDependencies {
    fun injects(target: BaseApplication)
}

@Module
private interface ComponentDependenciesModule {
    @Binds
    fun provideCatsComponentDependencies(component: AppComponent): CatsComponentDependencies
}
